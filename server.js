const config = require("./config.json"),
  WebSocket = require("ws"),
  shortid = require("shortid"),
  {UMap} = require("./utils"),
  {Arena, ArenaObject, BattleBot} = require("./BattleBot");

const wss = new WebSocket.Server(config);

console.log("I'm listening!");

const clients = new UMap();

const arenas = new Map();

/*********************
**  EVENT HANDLERS  **
*********************/

wss.on("connection", (socket) => {
  socket.sendEvent = (type = "event", payload = {}) => socket.send(JSON.stringify({type, payload}));

  socket.token = shortid.generate();

  clients.set(socket.token, socket);
console.log("New client connected:", socket.token);
  socket.sendEvent("ready", socket.token);

  /************************************
  **  Message to Command Conversion  **
  ************************************/
  socket.on("message", data => {
    try {
      socket.isAlive = true;
      data = JSON.parse(data);
      if (data.token !== socket.token) socket.sendEvent("error", {error: "Incorrect token.", data});
      else if (!data.type) socket.sendEvent("error", {error: "Message type undefined.", data});
      else if (data.payload === undefined) socket.sendEvent("error", {error: "Message payload undefined.", data});
      else socket.emit(data.type, data.payload);
    } catch(e) {
      console.error(Date());
      console.error(e);
      socket.sendEvent("error", {error: e.message, data});
    }
  })
  .on("error", error => socket.sendEvent("error", error));

  /*****************************
  **  Client Command Handler  **
  *****************************/
  socket
  .on("echo", data => socket.sendEvent("echo", data))
  .on("join", (auth) => {
    socket.name = auth.name;

    socket.bot = new BattleBot(socket.name);
    socket.bot.socket = socket;

    if (!arenas.has("Arena")) arenas.set("Arena", new Arena(10, .1, 4, "Arena"));
    arenas.get("Arena").add(socket.bot);

    socket.sendEvent("joined", {
      size: socket.bot.arena.size,
      capacity: socket.bot.arena.capacity,
      obstacles: socket.bot.arena.obstacleList,
      clientBot: socket.bot.state
    });

    if (socket.bot.arena.bots.size > 1) {
      let results = socket.bot.arena.resolveTurn();
      for (const [token, bot] of socket.bot.arena.bots) {
        bot.socket.sendEvent("state", results.get(bot.id));
      }
    }
  })
  .on("turn", turnData => {
    socket.bot.arena.queueTurn(socket.bot, turnData);
    if (socket.bot.arena.bots.filter(b => b.active).size == socket.bot.arena.actions.size) {
      let results = socket.bot.arena.resolveTurn();

      if (socket.bot.arena.bots.size < 2) {
        let arenaId = socket.bot.arena.id;

        for (const [token, client] of clients) {
          client.sendEvent("end", {
            "winner": (socket.bot.arena.bots.size == 1 ? socket.bot.arena.bots.first().name : null),
            rounds: socket.bot.arena.turn
          });
          client.close();
        };

        arenas.delete(arenaId);
      } else {
        for (const [token, client] of clients) {
          if (client.bot) {
            let update = results.get(client.bot.id) || results.get("inactive");
            client.sendEvent("state", update);
          }
        };
      }
    }
  });

  /*******************************
  **  Handle Broken Connection  **
  *******************************/
  socket
  .on("pong", () => { socket.isAlive = true; })
  .on("close", () => {
    if (socket.bot && socket.bot.arena) socket.bot.arena.destroy(socket.bot);
    clients.delete(socket.token);
  });

});

/*********************
**  Heartbeat Ping  **
*********************/
const interval = setInterval(function ping() {
  for (const socket of wss.clients) {
    if (socket.isAlive === false) return socket.terminate();

    socket.isAlive = false;
    socket.ping();
  };
}, 10000);
