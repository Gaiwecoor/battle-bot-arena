class UMap extends Map {
  constructor(data) {
    super(data);
  }

  array() {
    const values = Array(this.size);
    let i = 0;
    for (const [key, value] of this) {
      values[i++] = value;
    }
    return values;
  }

  clone() {
    const cloned = new UMap();
    for (const [key, value] of this) {
      cloned.set(key, value);
    }
    return cloned;
  }

  filter(fn) {
    const filtered = new UMap();
    for (const [key, value] of this) {
      if (fn(value, key)) filtered.set(key, value);
    }
    return filtered;
  }

  find(fn) {
    for (const [key, value] of this) {
      if (fn(value, key)) return value;
    }
    return undefined;
  }

  first(n) {
    if (n === undefined) {
      for (const [key, value] of this) return value;
    } else {
      const values = Array(Math.min(n, this.size));
      if (n == 0) return values;
      let i = 0;
      for (const [key, value] of this) {
        values[i++] = value;
        if (i >= values.length) return values;
      }
      return values;
    }
  }

  map(fn) {
    const mapped = Array(this.size);
    let i = 0;
    for (const [key, value] of this) {
      mapped[i++] = fn(value, key);
    }
    return mapped;
  }

  reduce(fn, init) {
    const accumulator = init;
    for (const [key, value] of this) {
      if (accumulator == undefined) {
        accumulator = value;
        continue;
      }
      accumulator = fn(accumulator, value, key, this);
    }
    return accumulator;
  }
}

class USet extends Set {
  constructor(data) {
    super(data);
  }

  array() {
    const values = Array(this.size);
    let i = 0;
    for (const value of this) {
      values[i++] = value;
    }
    return values;
  }

  clone() {
    const cloned = new USet();
    for (const value of this) {
      cloned.add(value);
    }
    return cloned;
  }

  filter(fn) {
    const filtered = new USet();
    for (const value of this) {
      if (fn(value)) filtered.add(value);
    }
    return filtered;
  }

  find(fn) {
    for (const value of this) {
      if (fn(value)) return value;
    }
    return undefined;
  }

  first(n) {
    if (n === undefined) {
      for (const value of this) return value;
    } else {
      const values = Array(Math.min(n, this.size));
      if (n == 0) return values;
      let i = 0;
      for (const value of this) {
        values[i++] = value;
        if (i >= values.length) return values;
      }
      return values;
    }
  }

  map(fn) {
    const mapped = Array(this.size);
    let i = 0;
    for (const value of this) {
      mapped[i++] = fn(value);
    }
    return mapped;
  }

  reduce(fn, init) {
    const accumulator = init;
    for (const value of this) {
      if (accumulator === undefined) {
        accumulator = value;
        continue;
      }
      accumulator = fn(accumulator, value, this);
    }
    return accumulator;
  }
}

module.exports = {
  UMap,
  USet
};
