/**************************************************************
**  A sample client, built by extending the WebSocket client **
**************************************************************/

const config = require("./config.json"),
  WebSocket = require("ws");

class BattleBotClient extends WebSocket {
  constructor(serverAddress, options = {}) {
    super(serverAddress);

    this.matchTurn = 0;

    this.on("message", data => {
      try {
        data = JSON.parse(data);
        if (data.type && data.payload) this.emit(data.type, data.payload);
        else this.emit("error", {error: "Unrecognized event.", data});
      } catch(error) { this.emit("error", {error: error.message, data}); }
    })
    .on("ready", token => {
      this.token = token;
    });
  }

  echo(data) {
    return this.send("echo", data);
  }

  join(data) {
    return this.send("join", data);
  }

  send(type = "event", payload = {}) {
    super.send(JSON.stringify({type, token: this.token, payload}));
    return this;
  }

  turn(data) {
    data.turn = this.matchTurn;
    return this.send("turn", data);
  }
}

const client = new BattleBotClient(config.serverAddress);

client.on("open", () => {
  console.log("Connected");
})
.on("close", () => console.log("Disconnected."))
.on("echo", console.log)
.on("end", (data) => {
  console.log(data);
  client.close();
})
.on("error", console.error)
.on("joined", console.log)
.on("ready", (id) => {
  console.log("Ready with token:", id);
})
.on("state", state => {
  console.log(state);
  client.matchTurn = state.nextTurn;
});

module.exports = {BattleBotClient, client};
