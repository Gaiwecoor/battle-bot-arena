const shortid = require("shortid");
const Jimp = require("jimp");
const {UMap, USet} = require("./utils");
var obst, body, turret, scanner;
var ready = Promise.all([
  Jimp.read("./images/circle.png"),
  Jimp.read("./images/body.png"),
  Jimp.read("./images/turret.png"),
  Jimp.read("./images/scanner.png")
]).then(async (r) => {
  [obst, body, turret, scanner] = r;
});

function draw(self, scale) { return async function(destination = "./exports/map.png") {
  try {
    await ready;
    if (self.updateMap) {
      self.map = new Jimp(scale * self.size, scale * self.size, 0xffffffff);

      for (const [k, obstacle] of self.obstacles) {
        let dot = obst.clone().resize(2 * obstacle.r * scale, 2 * obstacle.r * scale);
        self.map.blit(dot, (obstacle.x - obstacle.r) * scale, (self.size  - obstacle.y - obstacle.r) * scale);
      }
      self.updateMap = false;
    }

    const img = self.map.clone();

    for (const [k, bot] of self.bots) {
      if (!bot.img.body) bot.img.body = body.clone().color([{apply: "spin", params: [bot.color]}]);
      if (!bot.img.turret) bot.img.turret = turret.clone().color([{apply: "spin", params: [bot.color]}]);
      if (!bot.img.scanner) bot.img.scanner = scanner.clone().color([{apply: "spin", params: [bot.color]}]);

      let tank = new Jimp(scale * bot.r * 2, scale * bot.r * 2, 0xffffff00)
      .blit(bot.img.body.clone().rotate(bot.heading, false), 0, 0)
      .blit(bot.img.turret.clone().rotate(bot.turret, false), 0, 0)
      .blit(bot.img.scanner.clone().rotate(bot.scanner, false), 0, 0);
      img.blit(tank, (bot.x - bot.r) * scale, (self.size - bot.y - bot.r) * scale);
    }

    await img.writeAsync(destination);
  } catch(e) { console.error(e); }
}}

class Point {
  constructor(x, y) {
    if (x instanceof Point) {
      this.x = x.x;
      this.y = x.y;
    } else {
      this.x = x;
      this.y = y;
    }
  }

  d(pt) { // Delta coordinates (x, y)
    // Cartesian Delta Coordinates
    const x = (pt.x - this.x);
    const y = (pt.y - this.y);

    // Polar Bearing and Range
    let a = Math.atan(x / y) * 180 / Math.PI;
    if (x < 0 && y > 0) a += 360; // Quadrant 4
    else if (y < 0) a += 180; // Quadrants 2 & 3

    const bearing = a;
    const r = Math.sqrt((x ** 2) + (y ** 2));

    return {x, y, bearing, r};
  }

  inArc(pt, start, sweep = 0, range = undefined) { // Determines whether a point is within an arc
    start = (start + 360) % 360;
    let {bearing, r} = this.d(pt);

    if (range && range > r) return false;
    if (bearing == start) return true;
    else if (((start + sweep) >= 0) && ((start + sweep) < 360) && (sweep > 0)) {
      // CW sweep does not cross 0
      return ((bearing >= start) && (bearing <= (start + sweep)));
    } else if (((start + sweep) >= 0) && ((start + sweep) < 360) && (sweep < 0)) {
      // CCW sweep does not cross 0
      return ((bearing <= start) && (bearing >= (start + sweep)));
    } else if ((start + sweep) >= 360) {
      // sweep CW across 0
      return ((bearing >= start) || (bearing <= ((start + sweep) % 360)));
    } else if ((start + sweep) < 0) {
      // sweep CCW across 0
      return ((bearing <= start) || (bearing >= (start + sweep + 360)));
    } else return false;
  }
}

class ArenaObject extends Point {
  constructor(x, y, r = .5) {
    super(x, y);
    if (x instanceof ArenaObject) {
      this.id = x.id;
      this.r = x.r;
      this.hp = x.hp;
      this.arena = x.arena;
    } else {
      this.id = shortid.generate();
      this.r = r;
      this.hp = 40 * (this.r ** 2);
      this.arena = null;
    }
    this.type = "ArenaObject";
  }

  addToArena(arena) {
    this.arena = arena;
    arena.add(this);
    return this;
  }

  arcRadius(ao) {
    return Math.asin(ao.r / this.d(ao).r) * 180 / Math.PI;
  }

  collidesWith(ao) {
    return (this.d(ao).r < (this.r + ao.r));
  }

  inArc(pt, start, sweep = 0, range = undefined) {
    if (pt instanceof ArenaObject) {
      let delta = this.arcRadius(pt);
      if (sweep >= 0) return super.inArc(pt, start - delta, sweep + (2 * delta), range);
      else if (sweep < 0) return super.inArc(pt, start + delta, sweep - (2 * delta), range);
    } else return super.inArc(pt, start, sweep, range);
  }

  nearPoint(ao, angle) {
    const {x, y} = this.d(ao);
    const r = ao.r;

    let b = (x * Math.sin(angle * Math.PI / 180) + y * Math.cos(angle * Math.PI / 180));
    let np = b - Math.sqrt((b ** 2) + (r ** 2) - (x ** 2) - (y ** 2));
    return np;
  }

  setLocation(x, y = null) {
    if (x instanceof Point) {
      this.x = x.x;
      this.y = x.y;
    } else {
      this.x = x;
      this.y = y;
    }
    return this;
  }

  setHP(hp) {
    this.hp = hp;
    return this;
  }

  get export() {
    return {
      type: this.type,
      id: this.id,
      hp: this.hp,
      x: this.x,
      y: this.y,
      r: this.r
    }
  }
}

class BattleBot extends ArenaObject {
  constructor(name = "Some Bot") {
    super(0, 0);
    this.hp = 100;
    this.name = name;
    this.type = "BattleBot";
    this.color = Math.random() * 360;
    this.heading = Math.random() * 360;
    this.turret = Math.random() * 360;
    this.scanner = Math.random() * 360;
    this.damage = [];
    this.scanned = new USet();
    this.scannedBy = new USet();
    this.img = {};

    // Constants
    this.ATK_CANNON = 1;
    this.ATK_FLAME = 2;
    this.ATK_MORTAR = 4;
  }

  get active() {
    return this.hp > 0;
  }

  attack(weapon) {
    const {type, bearing, range} = weapon;
    let targets = this.arena.all.filter(ao => ao.id != this.id);
    let value = 0;

    if (type & this.ATK_CANNON) {
      value = 40;
      targets = targets.filter(ao => this.inArc(ao, bearing));

      if (targets.length > 0) {
        let target = targets.reduce((a, c) => (this.nearPoint(c, bearing) < this.nearPoint(a, bearing) ? c : a));
        targets = new USet([target]);
      }
    } else if (type & this.ATK_FLAME) {
      value = 30;
      targets = targets.filter(ao => this.inArc(ao, bearing - 15, 30) && this.d(ao).r <= 3);
    } else if (type & this.ATK_MORTAR) {
      value = 20;
      let effect = new ArenaObject(this.x + (Math.sin(bearing * Math.PI / 180) * range), this.y + (Math.cos(bearing * Math.PI / 180) * range), 1);
      targets = targets.filter(ao => effect.collidesWith(ao));
    }
    if (targets.size > 0) {
      for (const target of targets) {
        target.hp -= value;
        if (target instanceof BattleBot) target.damage.push({type, value});
      }
    }
  }

  checkCollisions() {
    this.collide = false;
    for (const ao of this.arena.all) {
      if (this.id == ao.id) continue;
      else if (this.collidesWith(ao)) {
        let command = this.arena.actions.get(this.id);
        if (command && command.move) command.move.type = 0;
        this.collide = true;
      }
    }
    return this.collide;
  }

  get export() {
    let value = super.export;
    value.name = this.name;
    value.color = this.color;
    value.heading = this.heading;
    value.turret = this.turret;
    value.scanner = this.scanner;
    return value;
  }

  move(speed, ticks = 8) {
    speed = Math.max(speed, 0);
    speed = Math.min(4, speed);

    let dx = speed * Math.sin(this.heading * Math.PI / 180) / ticks;
    let dy = speed * Math.cos(this.heading * Math.PI / 180) / ticks;

    this.x += dx;
    this.y += dy;

    if ((this.x > this.arena.size) || (this.x < 0) || (this.y > this.arena.size) || (this.y < 0)) {
      this.x = Math.min(this.x, this.arena.size);
      this.x = Math.max(this.x, 0);
      this.y = Math.min(this.y, this.arena.size);
      this.y = Math.max(this.y, 0);

      this.arena.actions.get(this.id).move.type = 0;
    }

    return this;
  }

  setColor(color) {
    this.color = color;
    return this;
  }

  get state() {
    return {
      id: this.id,
      hp: this.hp,
      x: this.x,
      y: this.y,
      r: this.r,
      name: this.name,
      heading: this.heading,
      scanner: this.scanner
    }
  }
}

class Arena {
  constructor(size, density, capacity, id) {
    this.id = id || shortid.generate();
    this.size = size;
    this.obstacles = new UMap();
    this.bots = new UMap();
    //this.destroyed = new UMap();
    this.capacity = capacity;
    this.draw = draw(this, 100);
    this.updateMap = true;
    this.map = null;
    this.active = false;
    this.actions = new UMap();
    this.shots = new USet();
    this.destruction = {
      bots: new USet(),
      obstacles: new USet()
    };
    this.turn = 0;

    this.populate(density);
  }

  get export() {
    return ({
      size: this.size,
      obstacles: Array.from(this.obstacles.values()).map(i => i.export),
      bots: Array.from(this.bots.values()).map(i => i.export)
    })
  }

  add(ao) {
    if (ao instanceof BattleBot) {
      let collide;
      do {
        ao.setLocation(Math.random() * this.size, Math.random() * this.size);
        collide = false;

        for (const [k, obstacle] of this.obstacles) {
          if (ao.collidesWith(obstacle)) {
            collide = true;
            break;
          }
        }
        if (!collide) {
          for (const [k, bot] of this.bots) {
            if (ao.collidesWith(bot)) {
              collide = true;
              break;
            }
          }
        }
      } while (collide);
      this.bots.set(ao.id, ao);
    } else this.obstacles.set(ao.id, ao);
    ao.arena = this;
    return this;
  }

  get all() {
    let everything = new USet();
    for (const [id, bot] of this.bots) {
      everything.add(bot);
    }
    for (const [id, obstacle] of this.obstacles) {
      everything.add(obstacle);
    }
    return everything;
  }

  checkDamage() {
    for (const [id, bot] of this.bots) {
      if (bot.hp <= 0) this.destroy(bot);
    }
    return this;
  }

  destroy(ao) {
    if (ao instanceof BattleBot) {  // Destroy a Bot
      let rubble = (new ArenaObject(ao)).setHP(ao.hp + 40);
      //this.destroyed.add(ao);
      this.bots.delete(ao.id);
      this.actions.delete(ao.id);
      this.destruction.bots.add(rubble);
      if (rubble.hp > 0) {
        this.add(rubble);
      } else {
        this.destruction.obstacles.add(rubble.id);
      }
    } else {  // Destroy an Obstacle
      this.obstacles.delete(ao.id);
      this.destruction.obstacles.add(ao.id);
    }
    return this;
  }

  get obstacleList() {
    const list = Array(this.obstacles.size);
    let i = 0;
    for (const [key, value] of this.obstacles) {
      list[i++] = {
        id: value.id,
        hp: value.hp,
        x: value.x,
        y: value.y,
        r: value.r
      }
    }
    return list;
  }

  populate(density) {
    for (let i = this.obstacles.size; i < Math.floor(density * (this.size ** 2)); i++) {
      this.add(new ArenaObject(Math.random() * this.size, Math.random() * this.size, ((Math.random() * 1.5) + .5) / 2));
    }
    return this;
  }

  queueTurn(bot, data) {
    if ((data.turn === this.turn) && this.bots.has(bot.id)) {
      this.actions.set(bot.id, data);
    }
    return this;
  }

  resolveTurn() {
    // Move over the course of eight ticks
    for (let tick = 0; tick < 8; tick++) {
      for (const [botId, command] of this.actions) {
        let bot = this.bots.get(botId);
        if (command.move && (command.move.type & 1)) {
          bot.move(command.move.value);
        } else if (command.move && (command.move.type & 2) && tick == 0) {
          bot.heading = command.move.value || 0;
        }
      }
      for (const [id, bot] of this.bots)
        bot.checkCollisions();
    }

    // Deal collision damage
    for (const [id, bot] of this.bots) {
      if (bot.collide) {
        bot.hp -= 10;
        bot.damage.push({type: 0, value: 10});
        bot.collide = false;
      }
    }

    this.checkDamage();

    // Fire weapons
    for (const [botId, command] of this.actions) {
      if (command.weapon && command.weapon.type) {
        let bot = this.bots.get(botId);
        this.shots.add(bot);
        bot.attack(command.weapon);
      }
    }

    this.checkDamage();

    // Scan
    for (const [botId, command] of this.actions) {
      let bot = this.bots.get(botId);
      if (command.scan && bot.hp > 0) {
        for (const [id, target] of this.bots) {
          if ((bot.id != id) && bot.inArc(target, bot.scanner, command.scan, Math.sqrt(360 / command.scan))) {
            bot.scanned.add(target);
            target.scannedBy.add(bot);
          }
        }
        bot.scanner = (bot.scanner + command.scan + 360) % 360;
      }
    }

    // Results
    let botsDestroyed = this.destruction.bots.map(destroyed => ({
      id: destroyed.id,
      hp: destroyed.hp,
      x: destroyed.x,
      y: destroyed.y,
      r: destroyed.r
    }));
    let objectsDestroyed = this.destruction.obstacles.map(destroyed => destroyed.id);

    const results = new UMap([["inactive", {
      turn: this.turn,
      nextTurn: this.turn + 1,
      playersRemaining: this.bots.size,
      botsDestroyed,
      objectsDestroyed,
    }]]);

    for (const [id, bot] of this.bots) {
      results.set(bot.id, {
        turn: this.turn,
        nextTurn: this.turn + 1,
        playersRemaining: this.bots.size,
        clientBot: bot.state,
        damage: bot.damage,
        shots: this.shots
          .filter(shot => shot.id != bot.id)
          .map(shot => bot.d(shot).bearing),
        botsDestroyed,
        objectsDestroyed,
        scanResults: bot.scanned.map(scan => ({
          name: scan.name,
          bearing: bot.d(scan).bearing,
          r: bot.d(scan).r
        })),
        scannedBy: bot.scannedBy.map(scanner => bot.d(scanner).bearing)
      });

      bot.scanned.clear();
      bot.scannedBy.clear();
      bot.damage = [];
    }

    this.actions.clear();
    this.destruction.bots.clear();
    this.destruction.obstacles.clear();
    this.shots.clear();
    this.turn++;

    return results;
  }
}

module.exports = {
  Arena,
  ArenaObject,
  BattleBot,
  Point
};
