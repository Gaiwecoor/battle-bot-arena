# Battle Bot Arena

  The purpose of Battle Bot Arena is to provide a server which will run a battle simulation for user-provided bots to battle. The users write the client in whichever language they like; any language that can use [websockets](https://en.wikipedia.org/wiki/WebSocket) is supported. Most modern languages will have an easily accessible library for use.

  While a client may be built to accept manual input, the intent is to control the bots via programmed algorithms.

---

## Game Basics

  Each connected client controls a bot that will be placed in an arena to battle with other bots. The bot will receive a map of the arena (including all obstacles, their locations, and HP) and its location, but not the location of the other bots.

  Each turn has three segments: bot movement, turret, and scanning.

### Bot Movement

  The bot body has a radius of 0.5 (diameter 1) and may have a heading from 0° to 360°, with 0° being North (positive values indicating a clockwise rotation). The origin is located at the southwest corner of the map. Each turn, the bot may either (1) change it heading; or (2) move with a speed of 0-4. The bot has 100 HP and will be destroyed if reduced to 0 HP by weapon fire or colliding with other bots or obstacles.

### Turret

  The turret has a bearing of 0° to 360° (independent of the body or scanner) with 0° being North (positive values indicating a clockwise rotation). This will determine the direction of fire if and when you fire your weapons. Three weapons are available:

  * **Cannon:** [Type 1] The cannon is the most powerful of the bot's weapons (dealing 40 damage), firing in a straight line along the turret bearing, up to its maximum range of 4. The cannon only damages the first target it hits.
  * **Scattergun:** [Type 2] The scattergun can affect a greater area than the cannon (30° spread up to a range of 3) but is slightly weaker (dealing 30 damage).
  * **Mortar:** [Type 4] The mortar is the most versatile weapon, allowing the bot to attack at range. It creates a circular burst (dealing 20 damage) with a diameter of 3 and the center of effect at a range of up to 5.

### Scanner

  The scanner has a bearing of 0° to 360° (independent of the body or turret) with 0° being North (positive values indicating a clockwise rotation). When moving, the scanner can sweep an area, providing the location of enemies within the arc. While the arc can be as wide as the bot desires, the range of the arc will decrease as the arc gets wider, keeping the total sweep area the same.

---

## Message Format

### Server (to Client) Messages (Events)

  The server sends JSON-formatted strings to communicate with the client upon certain events occurring. The strings sent from the server contain two parts:

  1. The `type` of the event; and
  2. The data `payload`. Payloads are typically objects, but may also be numbers, strings, or arrays. See particular event details for more information.

  ```
  {
      "type": "eventType",
      "payload": {}
  }
  ```

### Client (to Server) Messages (Commands)

  When sending data to the server, the client should send a JSON-formatted string. The strings sent from the client contain three parts:

  1. The `token` provided by the server upon the initial connection;
  2. The `type` of the message being sent; and
  3. The data `payload`. Payloads are typically objects, but may also be numbers, strings, or arrays. See particular event details for more information.

  ```
  {
      "token": "YOURCLIENTTOKEN",
      "type": "messageType",
      "payload": {}
  }
  ```

---

## Connecting and Joining an Arena

  Upon connecting to the server, the client and server exchange a few commands and events. The following list shows the order of events and commands. See the below Event and Command reference for more details on each.

  * The Client connects to the server.
  * The Server sends the `ready` event, with the client's `token`.
  * The Client sends the `join` command.
  * The Server sends the `joined` event, with arena information.
  * The Server sends the `state` event once the match begins.
  * The Client responds with a `turn` command.
  * The Server and Client exchange `state` and `turn` cycles until the match is concluded.
  * The Server sends the `end` event.

## Maintaining a connection and ping

  WebSocket spec-compliant libraries will automatically respond to a `ping` with a `pong`. The server sends a `ping` every 10 seconds to verify the connection is still active. **Most clients should not need to worry about any further connection verification.** If you are using a non-compliant library (i.e. the client does not respond to the `ping`), the server must receive a message from the client at least once every 10 seconds or the connection will be terminated.

---

## Event Reference

### Event: `end`

  * **Sent:** The `end` event will be sent when a battle ends.
  * **Payload:** The payload will be an array of bot information, including standings, number of kills, rounds before destruction, and who destroyed it.

  ```
  {
      "winner": String,
      "rounds": Int
  }
  ```

### Event: `error`

  * **Sent:** The `error` event will be sent upon a server error in response to malformed message.
  * **Payload:** The payload will be an object containing a brief error message and the data sent by the client resulting in the error.

  ```
  {
      "error": String,
      "data": Object
  }
  ```

### Event: `joined`

  * **Sent:** The `joined` event will be sent upon the client successfully joining an arena.
  * **Payload:** The payload will be an object containing arena information:

  ```
  {
      "size": Number,
      "capacity", Number
      "obstacles": [
          {
              "id": String,
              "hp": Number,
              "x": Number,
              "y": Number,
              "r": Number
          }
      ],
      "clientBot": {
          "id": String,
          "hp": Number,
          "x": Number,
          "y": Number,
          "r": Number,
          "name": String,
          "heading": Number,
          "scanner": Number
      }
  }
  ```

### Event: `ready`

  * **Sent:** The `ready` event will be sent upon the client establishing a connection with the server.
  * **Payload:** The payload will be a string, containing the unique client token, which will be required in future messages to the server.

### Event: `state`

  * **Sent:** The `state` event will be sent at the beginning of the match and after each turn is processed.
  * **Payload:** The payload is an object containing your bots position and orientation as well as the following information (if applicable): an array of damage taken and type (collision or weapon damage), an array of bearings to any shots fired by other bots, an array of bots destroyed (by id number), an array of any objects/obstacles destroyed (by id number), an array of scan results (if applicable), and an array of bearings from which you were scanned (if applicable).

  ```
  {
      "turn": Int,
      "nextTurn": Int,
      "playersRemaining": Int,
      "clientBot": {
          "id": String,
          "hp": Number,
          "x": Number,
          "y": Number,
          "r": Number,
          "name": String,
          "heading": Number,
          "scanner": Number
      },
      "damage": [
          {
              "type": Int,
              "value": Int
          }
      ],
      "shots": [ Number ],
      "botsDestroyed": [
          {
              "id": String,
              "hp": Number,
              "x": Number,
              "y": Number,
              "r": Number
          }
      ],
      "objectsDestroyed": [ String ],
      "scanResults": [
          {
              "name": String,
              "bearing": Number,
              "r": Number
          }
      ],
      "scannedBy": [ Number ]
  }
  ```

---

## Client Command Reference

### Command: `join`

  *Note:* In early stages, all activity will take place within a single room. All fields except `name` and `battles` will be ignored. Matches will start 5 minutes from the time two clients connect.

  * **Send:** Send a `join` message after receiving the `ready` event to connect to an arena.
  * **Payload:** The payload is an object containing room, password, and client name information. If a room does not yet exist, one will be created. The match will begin at the earlier of the room reaching capacity (default: `4`, min: `2`) or the start date (default: 5 min from room creation). The client will remain connected for `battles` matches, if provided.

  ```
  {
      "room": String,
      "pwd": String,
      "name": String,
      "battles": Int,
      "team": Int,
      "capacity": Int,
      "start": Date
  }
  ```

### Command: `turn`

  * **Send:** Send a `turn` message after receiving the `state` event.
  * **Payload:** The payload includes the turn number, move, weapon, and scan values.
    - `turn`: Integer for the turn number, provided by the `nextTurn` property of the previous `state` event.
    - `move`: An object containing the following movement properties:
        * `type`: Integer for the type of move:
            - 0 (default): No movement.
            - 1: Move forward on current heading at a speed of `value` (0-4).
            - 2: Turn to `value` heading.
        * `value`: Speed or heading for the given `type`, above.
    - `weapon`: An object containing the following properties for weapon usage:
        * `type`: Weapon type used, if any.
            - 0 (default): No weapon.
            - 1: Canon.
            - 2: Scattergun.
            - 4: Mortar.
        * `bearing`: Bearing (0 being North) at which to fire the `weapon`.
        * `range`: if `weapon` is type 4 (Mortar), range at which to fire the Mortar.
    - `scan`: Relative motion in degrees to scan. A positive value indicates clockwise movement. For example, if your scanner is currently oriented at 40° and you pass a value of 20 to `scan`, you will scan the region between 40° and 60° with your end scanner position at 60°.

  ```
  {
      "turn": Int,
      "move": {
          "type": Int,
          "value": Number
      },
      "weapon": {
          "type": Int,
          "bearing": Number,
          "range": Number
      },
      "scan": Number
  }
  ```
